# Spaces Application
Share stuff from the Internet and comment in threads with groups of people.

## Configuration Requirements
This application requires the Google authentication tokens to be stored as environment variables with the following names:
* GOOGLE_CLIENT_ID
* GOOGLE_CLIENT_SECRET


## Setup local environment

1. `brew install sqlite3`
2. `bundle install`