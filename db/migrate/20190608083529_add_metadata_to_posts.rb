class AddMetadataToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :title, :string, after: :link
    add_column :posts, :description, :text, after: :title
    add_column :posts, :image, :string, after: :description
  end
end
