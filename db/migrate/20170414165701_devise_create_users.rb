class DeviseCreateUsers < ActiveRecord::Migration[4.2]
  def self.up
    create_table(:users) do |t|
      t.string :name, :default => ''
      t.string :picture, :default => ''
      t.string :email, :null => :false
      t.string :omniauth_uid, :null => false

      # t.rememberable
      # t.trackable
      # t.confirmable
      # t.token_authenticatable

      t.string :first_name
      t.string :last_name

      t.timestamps
    end

    add_index :users, :email,                :unique => true
    add_index :users, :omniauth_uid,         :unique => true
    # add_index :users, :authentication_token, :unique => true
  end

  def self.down
    drop_table :users
  end
end
