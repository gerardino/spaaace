# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


u = User.first

# Creating spaces
Space.delete_all

for i in 1..5 
    s = Space.new(name: "Test space #{i}", user: u)
    s.save
end

# Creating a couple posts
Post.delete_all
s = Space.first

for i in 1..3
    p = Post.new(space: s, user: u, link: "http://www.androidpolice.com/2017/12/22/android-8-0-oreo-rolling-verizon-moto-z2-force/")
    p.save
end
