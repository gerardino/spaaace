class Space < ApplicationRecord
  belongs_to :user
  has_many :memberships
  has_many :users, through: :memberships, as: :members

  has_many :posts

  def posts_count_text
    case posts.length
    when 0
      return 'No posts yet'
    when 1
      return 'One post'
    else
      return "#{ posts.length } posts"
    end
  end

  def can_user_post_here(user_id)
    users.where(id: user_id).present? || user.id == user_id
  end
end
