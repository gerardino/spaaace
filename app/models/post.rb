class Post < ApplicationRecord
  belongs_to :space
  belongs_to :user
  
  validates :link, url: true

  before_save :fill_metadata

  private

  def fill_metadata
    set_metadata if self.title.nil? || self.description.nil? || self.image.nil?
  end

  def set_metadata
    page = MetaInspector.new(self.link)
    self.title = page.title
    self.description = page.description
    self.image = page.images.best
  end
end
