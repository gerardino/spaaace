class User < ApplicationRecord
  # You can add other devise modules here as arguments, as in devise calls. :omniauthable module is always added
  # and forbidden devise modules are automatically removed (see GoogleAuthentication::ActsAsGoogleUser::FORBIDDEN_MODULES)
  # if you change this line, remember to edit the generated migration
  # acts_as_google_user

  devise :timeoutable, #:rememberable,
    :omniauthable, :omniauth_providers => [:google_oauth2]

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    # Uncomment the section below if you want users to be created if they don't exist
    unless user
        user = User.create(name: data["name"],
           email: data["email"],
           picture: data["image"],
           omniauth_uid: access_token["uid"]
           #password: Devise.friendly_token[0,20]
        )
    end
    user
 end
end
