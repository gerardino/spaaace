class HomeController < ApplicationController

  # GET /
  def index
    if user_signed_in?
      redirect_to spaces_path
    end
  end

end
